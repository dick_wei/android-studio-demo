package tjy.xa.cn.javalib.proxy;

/**
 * Created by wq on 2017/12/18.
 */

public class Main {
    public static void main(String[] args) {
        //创建目标对象
        IUserDao target = new Target();
        System.out.println("taget init" + target.getClass());
        // 给目标对象，创建代理对象
        IUserDao proxy = (IUserDao) new ProxyFactory(target).getProxyInstance();
        proxy.save();
    }


}
