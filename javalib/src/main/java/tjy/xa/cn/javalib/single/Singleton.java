package tjy.xa.cn.javalib.single;

/**
 * Created by wq on 2017/12/14.
 */

public class Singleton {
    private Singleton() {
    }

    public static Singleton getInstance() {
        return SingletonHolder.singleton;
    }

    static class SingletonHolder {
        public static Singleton singleton = new Singleton();
    }
}
