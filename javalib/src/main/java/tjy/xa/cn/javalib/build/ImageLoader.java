package tjy.xa.cn.javalib.build;

/**
 * Created by wq on 2017/12/14.
 */

public final class ImageLoader {
    private ImageLoader() {
    }

    public static ImageLoader getInstance() {
        return ImageLoaderHolder.instance;
    }

    public void init(ImageLoaderConfig config) {
        System.out.println(config.toString());
    }

    private static class ImageLoaderHolder {
        public static ImageLoader instance = new ImageLoader();
    }
}
