package tjy.xa.cn.javalib.build;

/**
 * Created by wq on 2017/12/14.
 */

public class ImageLoaderConfig {
    private int threadCount = 4;
    private int resId = 1000;
    private int errorId = 1001;


    public static class Builder {
        int threadCount;
        int resId = 200;
        int errorId = 500;

        public Builder setThreadCount(int threadCount) {
            this.threadCount = threadCount;
            return this;
        }

        public Builder setResId(int resId) {
            this.resId = resId;
            return this;
        }

        public Builder setErrorId(int errorId) {
            this.errorId = errorId;
            return this;
        }

        public ImageLoaderConfig create() {
            ImageLoaderConfig config = new ImageLoaderConfig();
            config.errorId = this.errorId;
            config.resId = this.resId;
            config.threadCount = this.threadCount;
            return config;
        }
    }

    @Override
    public String toString() {
        return "ImageLoaderConfig{" +
                "threadCount=" + threadCount +
                ", resId=" + resId +
                ", errorId=" + errorId +
                '}';
    }
}
