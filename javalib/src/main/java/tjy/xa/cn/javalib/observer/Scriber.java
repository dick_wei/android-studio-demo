package tjy.xa.cn.javalib.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by wq on 2017/12/18.
 */

public class Scriber implements Observer {
    private String name;

    public Scriber(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable observable, Object o) {
        System.out.println("收到通知:" + name);
    }

}
