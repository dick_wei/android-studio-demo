package tjy.xa.cn.javalib.base;

import java.io.File;

/**
 * Created by wq on 2017/12/18.
 */

public class LowerFileNameUtils {
    public static void main(String[] args) {
//        int[] number = new int[]{1, 3, 5};
//        count(1, 1, 1, 1, 2, 5);
//        testOverloading(1);//打印出A
//        testOverloading(1, 2);//打印出B
//        testOverloading(1, 2, 3);//打印出C

//        System.out.println(Super.num2);
//        System.out.println(Super.num1);
        String path = "D:\\tjy_project\\test\\hdpi";
        changeUpper2Small(path);
    }

    public static void changeUpper2Small(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < files.length; i++) {
            boolean isUpperCase = false;
            File file1 = files[i];
            String pathFile = file1.getAbsolutePath();
            int index = pathFile.lastIndexOf("\\");
            String oldPath = pathFile.substring(0, index + 1);
            String name = pathFile.substring(index + 1);
            sb.setLength(0);
            for (int j = 0; j < name.length(); j++) {
                char c = name.charAt(j);
                if (Character.isUpperCase(c)) {
                    isUpperCase = true;
                    c = Character.toLowerCase(c);
                    if ((j - 1) > 0 && name.charAt(j - 1) != '_') {
                        sb.append("_");
                    }
                }
                sb.append(c);
            }
            String newName = sb.toString();
            if (isUpperCase) {
                file1.renameTo(new File(oldPath, newName));
                System.out.println("原名称" + name);
                System.out.println("新名称" + newName);
            }
        }
    }

    public static void count(int... numbers) {
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        System.out.println("总和：" + sum);
    }

    private static void testOverloading(int i) {
        System.out.println("A");
    }

    private static void testOverloading(int i, int j) {
        System.out.println("B");
    }

    private static void testOverloading(int... more) {
        System.out.println("C");
    }


}
