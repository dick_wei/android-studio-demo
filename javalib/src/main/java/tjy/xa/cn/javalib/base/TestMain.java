package tjy.xa.cn.javalib.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wq on 2017/12/18.
 */

public class TestMain {
    public static void main(String[] args) {
        String dateStr = "2019-01-31";
//        long time=new Date(date).getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(dateStr);
            System.out.println(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


}
