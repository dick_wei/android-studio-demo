package tjy.xa.cn.javalib.observer;

import java.util.Observable;

/**
 * Created by wq on 2017/12/18.
 */

public class Publisher extends Observable {
    public void soldBook(String content) {
        System.out.println(content);
        setChanged();
        notifyObservers();
    }
}
