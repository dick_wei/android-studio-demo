package tjy.xa.cn.javalib.base;

/**
 * Created by wq on 2018/1/26.
 */

public class Person {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Person){
            Person p= (Person) o;
            return p.getName().equals(this.getName());
        }
        return super.equals(o);
    }
}
