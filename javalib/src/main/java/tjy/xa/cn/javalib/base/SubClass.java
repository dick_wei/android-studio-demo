package tjy.xa.cn.javalib.base;

/**
 * Created by wq on 2017/12/26.
 */

public class SubClass extends Super {
    public static final String num1 = "1";
    public static String num2 = "2";

    public SubClass() {
        super();
        System.out.print("SubClass");
    }

    static {
        System.out.println("sub init");
    }
}
