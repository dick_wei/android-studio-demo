package tjy.xa.cn.javalib.base;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by wq on 2017/12/18.
 */

public class Main {
    public static void main(String[] args) {

//        String path = "D:\\tjy_project\\test\\hdpi";
////        String path = "D:\\tjy_project\\code\\ElevatorMonitorTJY\\app\\src\\main\\res\\drawable-xxhdpi";
////        changeUpper2Small(path);
//        List<Person> personList=new ArrayList<>();
//        for(int i=0;i<4;i++){
//            personList.add(new Person("name"+i,i));
//        }
//        System.out.println("原size："+personList.size());
//        Person p1=new Person("name3",3);
//        if(!personList.contains(p1)){
//            personList.add(p1);
//        }
//        System.out.println("新size："+personList.size());
//        String path="sdcard\\photo\\2018:15:25 25-25-06.jpeg";
//        checkPath(path);
    }
public static void checkPath(String str){
    String regEx="-*:*";
    Pattern p = Pattern.compile(regEx);
    Matcher m = p.matcher(str);
    String newStr= m.replaceAll("").trim();
    System.out.print(newStr);
}
    public static void changeUpper2Small(String path) {
        File file = new File(path);
        File[] files = file.listFiles();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < files.length; i++) {
            boolean isUpperCase = false;
            File file1 = files[i];
            String pathFile = file1.getAbsolutePath();
            int index = pathFile.lastIndexOf("\\");
            String oldPath = pathFile.substring(0, index + 1);
            String name = pathFile.substring(index + 1);
            sb.setLength(0);
            for (int j = 0; j < name.length(); j++) {
                char c = name.charAt(j);
                if (Character.isUpperCase(c)) {
                    isUpperCase = true;
                    c = Character.toLowerCase(c);
                    if ((j - 1) > 0 && name.charAt(j - 1) != '_') {
                        sb.append("_");
                    }
                }
                sb.append(c);
            }
            String newName = sb.toString();
            if (isUpperCase) {
                file1.renameTo(new File(oldPath, newName));
                System.out.println("原名称" + name);
                System.out.println("新名称" + newName);
            }
        }
    }

    public static void count(int... numbers) {
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        System.out.println("总和：" + sum);
    }

    private static void testOverloading(int i) {
        System.out.println("A");
    }

    private static void testOverloading(int i, int j) {
        System.out.println("B");
    }

    private static void testOverloading(int... more) {
        System.out.println("C");
    }


}
