package tjy.xa.cn.javalib.single;

/**
 * Created by wq on 2017/12/14.
 */

public class Person {
    private volatile static Person instance;

    private Person() {
    }

    public static Person getInstance() {
        if (instance == null) {
            synchronized (Person.class) {
                if (instance == null) {
                    instance = new Person();
                }
            }
        }
        return instance;
    }
}
