package tjy.xa.cn.javalib.observer;

/**
 * Created by wq on 2017/12/18.
 */

public class Main {
    public static void main(String[] args){
        Scriber s1=new Scriber("a1");
        Scriber s2=new Scriber("a2");
        Scriber s3=new Scriber("a3");
        Publisher p1=new Publisher();
        p1.addObserver(s1);
        p1.addObserver(s2);
        p1.addObserver(s3);
        p1.soldBook("发布新书了");

    }
}
