package tjy.xa.cn.javalib.build;

public class Main {
    public static void main(String[] args) {
        System.out.print("hello world");
        ImageLoaderConfig config = new ImageLoaderConfig.Builder()
                .setErrorId(100)
                .setResId(200)
                .setThreadCount(5)
                .create();
        ImageLoader.getInstance().init(config);
    }
}
