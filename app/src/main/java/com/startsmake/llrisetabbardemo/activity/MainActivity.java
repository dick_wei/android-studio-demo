package com.startsmake.llrisetabbardemo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.startsmake.llrisetabbardemo.adapter.HomeFragmentAdapter;
import com.startsmake.llrisetabbardemo.fragment.PageFragment;
import com.startsmake.llrisetabbardemo.lib.MainNavigateTabBar;
import com.tabbar.demo.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG_PAGE_HOME = "首页";
    private static final String TAG_PAGE_CITY = "同城";
    private static final String TAG_PAGE_PUBLISH = "发布";
    private static final String TAG_PAGE_MESSAGE = "消息";
    private static final String TAG_PAGE_PERSON = "我的";
    private ImageView mIvAdd;
    private HomeFragmentAdapter adapter;
    private List<Fragment> fragmentList = new ArrayList<>();
    private String[] titles = new String[]{"首页", "同城", "消息", "我的"};
    private int[] normalImgs = new int[]{R.mipmap.comui_tab_home, R.mipmap.comui_tab_city, R.mipmap.comui_tab_message, R.mipmap.comui_tab_person};


    private MainNavigateTabBar tabBar;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabBar = (MainNavigateTabBar) findViewById(R.id.mainTabBar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mIvAdd= (ImageView) findViewById(R.id.iv_add);
        tabBar.onRestoreInstanceState(savedInstanceState);
        doBusiness(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        tabBar.onSaveInstanceState(outState);
    }


    public void doBusiness(Bundle saveInstance) {
        initFragment();
        adapter = new HomeFragmentAdapter(getSupportFragmentManager(),
                this, titles, fragmentList);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        mIvAdd.setOnClickListener(this);
        // 使用第三方tabbar
        tabBar.onRestoreInstanceState(saveInstance);
        int length = titles.length + 1;
        for (int i = 0; i < length; i++) {
            int position = i;
            if (i > 2) {
                position = i - 1;
            }
            if (i != 2) {
                tabBar.addTab(position, new MainNavigateTabBar.TabParam(normalImgs[position], R.mipmap.ic_refresh, titles[position]));
            } else {
                tabBar.addTab(i, new MainNavigateTabBar.TabParam(0, 0, ""));
            }
        }

        tabBar.setTabSelectListener(new MainNavigateTabBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(MainNavigateTabBar.ViewHolder holder) {
                int position = holder.tabIndex;
                viewPager.setCurrentItem(position);
            }
        });
        tabBar.setTabItemClickListener(new MainNavigateTabBar.OnTabItemClickListener() {
            @Override
            public void onTabClicked(MainNavigateTabBar.ViewHolder holder) {
                if (holder.tabIndex == tabBar.getCurrentSelectedTab()) {
                    showToast("刷新第" + holder.tabIndex + "个fragment");
                }
            }
        });
    }

    private void initFragment() {
        fragmentList.add(PageFragment.newInstance(0));
        fragmentList.add(PageFragment.newInstance(1));
        fragmentList.add(PageFragment.newInstance(2));
        fragmentList.add(PageFragment.newInstance(3));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add:
                showToast("点击了add");
                break;
        }
    }

    public void showToast(String text) {
        Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
