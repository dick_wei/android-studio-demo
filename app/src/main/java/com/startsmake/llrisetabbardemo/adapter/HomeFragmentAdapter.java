package com.startsmake.llrisetabbardemo.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.startsmake.llrisetabbardemo.fragment.PageFragment;

import java.util.List;

/**
 * Created by lenovo on 2017/6/12.
 */

public class HomeFragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private String[] titles;
    private List<Fragment> fragmentList;

    public HomeFragmentAdapter(FragmentManager fm, Context context, String[] titles, List<Fragment> fragmentList) {
        super(fm);
        this.context = context;
        this.titles = titles;
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        if (!fragmentList.isEmpty()) {
            return fragmentList.get(position);
        }
        return PageFragment.newInstance(0);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (titles != null) {
            return titles[position];
        }
        return "";
    }

    @Override
    public int getCount() {
        if(titles==null){
            return 0;
        }
        return titles.length;
    }
}
