package com.tabbar.demo;

import com.blankj.utilcode.util.Utils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.tabbar.libs.AppApplication;
import com.videogo.openapi.EZOpenSDK;

/**
 * Created by dick on 2017/8/23.
 */

public class MyApplication extends AppApplication {
    @Override
    public void onCreate() {
        super.onCreate();
//        initEZSDK();
        Utils.init(getApplicationContext());
        Fresco.initialize(this);
    }

    /**
     * 萤石SDK初始化
     */
    private void initEZSDK() {
        /**
         * sdk日志开关，正式发布需要去掉
         */
        EZOpenSDK.showSDKLog(true);
        /**
         * 设置是否支持P2P取流,详见api
         */
        EZOpenSDK.enableP2P(false);
        /**
         * APP_KEY请替换成自己申请的---需要在平台上申请
         */
        EZOpenSDK.initLib(this, "APP_KEY", "");
    }
}
