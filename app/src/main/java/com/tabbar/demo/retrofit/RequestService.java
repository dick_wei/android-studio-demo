package com.tabbar.demo.retrofit;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by wq on 2018/3/8.
 * 第一步，定义一个接口
 */

public interface RequestService {
    @GET("basil2style")
    Call<RequestBody> getString();
}
