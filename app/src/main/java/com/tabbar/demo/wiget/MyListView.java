package com.tabbar.demo.wiget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * action:自定义Listview，解决ScrollView中嵌套Listview显示不正常的问题
 * Created by bsh_kt on 2017/6/16.
 * des:
 */

public class MyListView extends ListView {
    private FloatScrollView scrollView;

    public void setScrollView(FloatScrollView scrollView) {
        this.scrollView = scrollView;
    }

    public MyListView(Context context) {
        super(context);
    }

    public MyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

//    // 分别记录上次滑动的坐标
//    private int mLastX = 0;
//    private int mLastY = 0;
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
//        int x = (int) ev.getX();
//        int y = (int) ev.getY();
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                scrollView.requestDisallowInterceptTouchEvent(true);
//                break;
//            case MotionEvent.ACTION_MOVE:
//                //如果listview第一个item可见
//                int deltaX = x - mLastX;
//                int deltaY = y - mLastY;
//                //如果scrollview滚动到顶部，或底部。事件交给listview处理，
//                break;
//            case MotionEvent.ACTION_UP:
//                break;
//            default:
//                break;
//        }
//        mLastX = x;
//        mLastY = y;
//        return super.dispatchTouchEvent(ev);
//    }
}
