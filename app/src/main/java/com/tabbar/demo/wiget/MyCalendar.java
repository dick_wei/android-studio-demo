package com.tabbar.demo.wiget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

import java.util.Calendar;
import java.util.Date;

public class MyCalendar extends LinearLayout implements OnClickListener {
    Context context;
    TextView tv_yearAndMonth;
    ImageButton ib_pre;
    ImageButton ib_next;
    ImageButton ib_back;
    GridLayout gl_calendar;
    View view;
    CalendarButton buttons[];
    int sideLength;
    String week[] = {"日", "一", "二", "三", "四", "五", "六"};
    Date date;
    Calendar calendar;
    int year, month, day;
    int res;
    private static final int COLUMN_COUNT = 7;
    private static final int ROW_COUNT = 5;

    public MyCalendar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();
    }

    public MyCalendar(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();
    }

    public MyCalendar(Context context) {
        super(context);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();
    }

    private void init() {
        this.view = LayoutInflater.from(context).inflate(R.layout.layout_mycalerdar, this);

        WindowManager ww = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        sideLength = ww.getDefaultDisplay().getWidth() / COLUMN_COUNT;

        //
        myFindViewById();

        //初始化日历按钮布局
        initCalendarLayout();

        //初始化按钮点击事件
        initButton();

    }

    private void initButton() {
        ib_pre.setOnClickListener(this);
        ib_next.setOnClickListener(this);
        ib_back.setOnClickListener(this);
    }

    private void myFindViewById() {
        tv_yearAndMonth = (TextView) view.findViewById(R.id.textView1);
        ib_pre = (ImageButton) view.findViewById(R.id.imageButton2);
        ib_next = (ImageButton) view.findViewById(R.id.imageButton3);
        ib_back = (ImageButton) view.findViewById(R.id.imageButton1);
        gl_calendar = (GridLayout) view.findViewById(R.id.gridLayout01);

        //
        buttons = new CalendarButton[COLUMN_COUNT * ROW_COUNT];

    }

    //根据传递过来的calendar，绘制当月的日历视图
    private void initCalendar(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = (calendar.get(Calendar.MONTH) + 1);
        int date = calendar.get(Calendar.DATE);

        //设置标题
        String todayStr = String.format("%04d年%02d月", calendar.get(Calendar.YEAR), (calendar.get(Calendar.MONTH) + 1));
        tv_yearAndMonth.setText(todayStr);

        //
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int currentMonthFirstDateInWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.set(Calendar.DAY_OF_MONTH, date);
        int currentMonthDaysSum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        calendar.roll(Calendar.MONTH, -1);
        int lastMonthDaysSum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int i;
        calendar.roll(Calendar.MONTH, 1);

        Log.w("月", currentMonthDaysSum + "天");
        Log.w("上月", lastMonthDaysSum + "天");

 /*
  * 设置日期
  */
        //设置本月
        ToolLog.w("currentMonthFirstDateInWeek", currentMonthFirstDateInWeek + "");
        ToolLog.w("currentMonthFirstDateInWeek%7111", currentMonthDaysSum + currentMonthFirstDateInWeek % 7 + "");

        for (i = currentMonthFirstDateInWeek % 7 + 7; i <= (currentMonthDaysSum + currentMonthFirstDateInWeek % 7 + 7 - 1) && i < COLUMN_COUNT * ROW_COUNT; i++) {
//            buttons[i].changeToState(Info.STATE_NORMAL_NORECORED);
            buttons[i].setDateText((i - currentMonthFirstDateInWeek % 7 - 7 + 1) + "");
            buttons[i].setDateTextColor(Color.BLACK);
            buttons[i].setEnabled(true);
            buttons[i].setDate(new Date(year, month, i - (currentMonthFirstDateInWeek % 7 + 7) + 1));
        }

        //设置上一个月
        for (i = 7; i < (currentMonthFirstDateInWeek % 7 + 7) && i < COLUMN_COUNT * ROW_COUNT; i++) {
//            buttons[i].changeToState(Info.STATE_NORMAL_NORECORED);
            buttons[i].setDateTextColor(Color.GRAY);
            buttons[i].setDateText(lastMonthDaysSum - (currentMonthFirstDateInWeek % 7 - i % 7) + 1 + "");
            buttons[i].setEnabled(false);
        }

        //设置下一个月
        for (i = currentMonthDaysSum + currentMonthFirstDateInWeek % 7 + 7; i < ROW_COUNT * COLUMN_COUNT; i++) {
//            buttons[i].changeToState(Info.STATE_NORMAL_NORECORED);
            buttons[i].setDateTextColor(Color.GRAY);
            buttons[i].setDateText((i - (currentMonthDaysSum + currentMonthFirstDateInWeek % 7 + 7) + 1) + "");
            buttons[i].setEnabled(false);
        }

        //设置当天
        if (year == this.year && month == this.month && day == this.day)
//            buttons[date + currentMonthFirstDateInWeek % 7 + 7 - 1].changeToState(Info.STATE_TODAY_NORECORED);

            //初始化界面
            if (res != 0)
                initLayout(res);

    }


    private void initCalendarLayout() {
        //设置行数，列数
        gl_calendar.setRowCount(ROW_COUNT);
        gl_calendar.setColumnCount(COLUMN_COUNT);


 /*
  * 添加按钮到布局
  */
        int i;
        int sum = ROW_COUNT * COLUMN_COUNT;

        //设置星期
        for (i = 0; i < COLUMN_COUNT; i++) {
            buttons[i] = new CalendarButton(context);
            buttons[i].setDateText(week[i]);
            buttons[i].setLayoutParams(new LayoutParams(sideLength, sideLength));
            gl_calendar.addView(buttons[i], i);
            buttons[i].setEnabled(false);
        }

        for (i = COLUMN_COUNT; i < sum; i++) {
            buttons[i] = new CalendarButton(context);
            buttons[i].setDateText("55");
            buttons[i].setLayoutParams(new LayoutParams(sideLength, sideLength));
            gl_calendar.addView(buttons[i], i);
        }


        //根据当月设置情况
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DATE);
        initCalendar(calendar);


    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            //上一个月
            case R.id.imageButton2: {
                calendar.roll(Calendar.MONTH, -1);
                initCalendar(calendar);
                break;
            }

            //下一个月
            case R.id.imageButton3: {
                calendar.roll(Calendar.MONTH, 1);
                initCalendar(calendar);
                break;
            }
        }
    }

    public void setOnClickButtonListener(OnClickListener l, int index) {
        buttons[index].setOnClickListener(l);
    }

    public Date getDate(int index) {
        return buttons[index].getDate();
    }

    public void setData(float f, int index) {
        buttons[index].tv_data.setText(String.format("%.1f", f));
        buttons[index].tv_data.setVisibility(TextView.VISIBLE);
    }

    public void initLayout(int res) {
//        switch (res) {
//            case Info.VIEW_WEIHT: {
//
//
//                break;
//            }
//
//            default: {
//                break;
//            }
//        }
    }

    public void setRes(int res) {
        this.res = res;
    }
}
