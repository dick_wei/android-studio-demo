package com.tabbar.demo.wiget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import android.widget.ListView;

import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

/**
 * Created by dick on 2017/10/18.
 */

public class MySwipeRefreshLayout extends SwipeRefreshLayout {
    private ListView mListView;
    private View mFooterView;
    private int mScaledTouchSlop;
    private boolean isLoading;
    private OnLoadListener mOnLoadListener;

    public MySwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public MySwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mFooterView = View.inflate(context, R.layout.foot_view, null);
// 表示控件移动的最小距离，手移动的距离大于这个距离才能拖动控件
        mScaledTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        ToolLog.w("ev", mScaledTouchSlop);
    }

    public void setmListView(ListView mListView) {
        this.mListView = mListView;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

//        if (mListView == null) {
//            if (getChildAt(0) instanceof ListView) {
//                mListView = (ListView) getChildAt(0);
//                setListViewOnScroll();
//            }
//        }
    }

    /**
     * 设置ListView的滑动监听
     */
    private void setListViewOnScroll() {

        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 移动过程中判断时候能下拉加载更多
                if (canLoadMore()) {
                    // 加载数据
                    loadData();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    private float mDownY, mUpY;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDownY = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                if (canLoadMore()) {
                    loadData();
                }
                break;
            case MotionEvent.ACTION_UP:
                mUpY = ev.getY();
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    private boolean canLoadMore() {
        // 1. 是上拉状态
        boolean condition1 = (mDownY - mUpY) >= mScaledTouchSlop;
        //  2 当前页面可见的item是最后一个条目
        boolean condition2 = false;
        if (mListView != null && mListView.getAdapter() != null) {
            ToolLog.w("ev", "lastVisiablePostion:" + mListView.getLastVisiblePosition());
            condition2 = (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1);
        }
        //3正在加载状态
        boolean condition3 = !isLoading;
        if (condition3) {
            ToolLog.w("ev", "不是正在加载状态");
        }
        ToolLog.w("ev", "cond1:" + condition1 + "\t con2;" + condition2 + "\tcon3:" + condition3);
        return condition1 && condition2 && condition3;
    }

    private void loadData() {
        System.out.println("加载数据...");
        if (mOnLoadListener != null) {
            // 设置加载状态，让布局显示出来
            setLoading(true);
            mOnLoadListener.onLoad();
        }

    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        if (isLoading) {
            mListView.addFooterView(mFooterView);
        } else {
            // 隐藏布局
            mListView.removeFooterView(mFooterView);
            // 重置滑动的坐标
            mDownY = 0;
            mUpY = 0;
        }
    }

    public interface OnLoadListener {
        void onLoad();
    }

    public void setOnLoadListener(OnLoadListener listener) {
        this.mOnLoadListener = listener;
    }
}
