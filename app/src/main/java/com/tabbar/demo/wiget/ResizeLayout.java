package com.tabbar.demo.wiget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.ScreenUtils;
import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

public class ResizeLayout extends LinearLayout {
    private static int count = 0;
    private final int keyHeight;
    private EditText editText;
    private int[] position = new int[2];
    private final int screenHeight;

    public ResizeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        //获取屏幕高度
        screenHeight = ScreenUtils.getScreenHeight();
        //阀值设置为屏幕高度的1/3
        keyHeight = screenHeight / 3;
        ToolLog.w("input", "screenHeight:" + screenHeight);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
//        ToolLog.w("input " + count++, "=>OnLayout called! l=" + l + ", t=" + t + ",r=" + r + ",b=" + b);
        if (editText == null) {
            editText = (EditText) this.findViewById(R.id.edt_text);
        }
        if (editText != null) {
            editText.getLocationOnScreen(position);
            ToolLog.w("input", "\t position on screenY:" + position[1]);
            if (listener != null)
                listener.onSoftKeyboardShown(position[1] < screenHeight * 0.7);
        }
    }

    public interface Listener {
        public void onSoftKeyboardShown(boolean isShowing);
    }

    private Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}