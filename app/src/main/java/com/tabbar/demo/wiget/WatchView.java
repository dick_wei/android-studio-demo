package com.tabbar.demo.wiget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by dick on 2017/11/6.
 */

public class WatchView extends View {
    private Paint paintCircle;
    private Paint paintDegreen;
    private Paint paintHour;
    private Paint paintMinute;
    private int mWidth = 0;
    private int mHeight = 0;

    public WatchView(Context context) {
        super(context);
        init(context);
    }

    public WatchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public WatchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        paintCircle = new Paint();
        paintCircle.setStyle(Paint.Style.STROKE);
        paintCircle.setAntiAlias(true);
        paintCircle.setStrokeWidth(5);

        paintDegreen = new Paint();
        paintDegreen.setStyle(Paint.Style.STROKE);
        paintDegreen.setAntiAlias(true);
        paintDegreen.setStrokeWidth(3);
        paintDegreen.setColor(Color.RED);

        paintHour = new Paint();
        paintHour.setStyle(Paint.Style.STROKE);
        paintHour.setAntiAlias(true);
        paintHour.setStrokeWidth(20);
        paintHour.setColor(Color.YELLOW);

        paintMinute = new Paint();
        paintMinute.setStyle(Paint.Style.STROKE);
        paintMinute.setAntiAlias(true);
        paintMinute.setStrokeWidth(10);
        paintMinute.setColor(Color.GREEN);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mWidth = getWidth();
        mHeight = getHeight();
        float radius = mWidth / 2.0f;
        canvas.drawCircle(radius, mHeight / 2, radius, paintCircle);
        //画表针
        for (int i = 0; i < 24; i++) {
            String degree = String.valueOf(i);
            if (i % 6 == 0) {
                paintDegreen.setStrokeWidth(5);
                paintDegreen.setTextSize(30);
                canvas.drawLine(radius, mHeight / 2 - radius,
                        radius, mHeight / 2 - radius + 60, paintDegreen);
                canvas.drawText(degree, radius - paintDegreen.measureText(degree) / 2,
                        mHeight / 2 - radius +90, paintDegreen);
            } else {
                paintDegreen.setStrokeWidth(3);
                paintDegreen.setTextSize(15);
                canvas.drawLine(radius, mHeight / 2 - radius,
                        radius, mHeight / 2 - radius + 30, paintDegreen);

                canvas.drawText(degree, radius - paintDegreen.measureText(degree) / 2,
                        mHeight / 2 - radius + 60, paintDegreen);
            }
            //旋转画布简化坐标运算
            canvas.rotate(15, mWidth / 2, mHeight / 2);


        }
        //画指针
        canvas.save();
        canvas.translate(radius,mHeight/2);
        canvas.drawLine(0,0,100,-100,paintHour);
        canvas.drawLine(0,0,100,-200,paintMinute);
        canvas.restore();

    }
}
