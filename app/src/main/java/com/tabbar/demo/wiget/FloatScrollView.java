package com.tabbar.demo.wiget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

/**
 * ============================================================
 * <p>
 * project name : TiantianFangFu
 * <p>
 * copyright ZENG HUI (c) 2015
 * <p>
 * author : HUI
 * <p>
 * QQ ��240336124
 * <p>
 * version : 1.0
 * <p>
 * date created : On July, 2015
 * <p>
 * description : ����λ�ü�����ScrollView
 * <p>
 * revision history :
 * <p>
 * ============================================================
 */
public class FloatScrollView extends ScrollView {
    private OnScrollListener onScrollListener;
    /**
     * 主要是用在用户手指离开MyScrollView，MyScrollView还在继续滑动，我们用来保存Y的距离，然后做比较
     */
    private int lastScrollY;

    public FloatScrollView(Context context) {
        super(context, null);
    }

    public FloatScrollView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public FloatScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * 设置滚动接口
     *
     * @param onScrollListener
     */
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (onScrollListener != null) {
            onScrollListener.onScroll(t, oldt);
        }
    }

    //    /**
//     * 用于用户手指离开MyScrollView的时候获取MyScrollView滚动的Y距离，然后回调给onScroll方法中
//     */
//    private Handler handler = new Handler() {
//
//        public void handleMessage(android.os.Message msg) {
//            int scrollY = getScrollY();
//
//            //此时的距离和记录下的距离不相等，在隔5毫秒给handler发送消息
//            if (lastScrollY != scrollY) {
//                lastScrollY = scrollY;
//                handler.sendMessageDelayed(handler.obtainMessage(), 5);
//            }
//            if (onScrollListener != null) {
//                onScrollListener.onScroll(scrollY);
//            }
//        }
//    };
//
//    /**
//     * 重写onTouchEvent， 当用户的手在MyScrollView上面的时候，
//     * 直接将MyScrollView滑动的Y方向距离回调给onScroll方法中，当用户抬起手的时候，
//     * MyScrollView可能还在滑动，所以当用户抬起手我们隔5毫秒给handler发送消息，在handler处理
//     * MyScrollView滑动的距离
//     */
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        if (onScrollListener != null) {
//            onScrollListener.onScroll(lastScrollY = this.getScrollY());
//        }
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_UP:
//                handler.sendMessageDelayed(handler.obtainMessage(), 20);
//                break;
//        }
//        return super.onTouchEvent(ev);
//    }

    /**
     * 滚动的回调接口
     */
    public interface OnScrollListener {
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         */
        public void onScroll(int y, int oldY);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        View childView = getChildAt(0);
        boolean intercepted = false;
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                intercepted = false;
                break;
            case MotionEvent.ACTION_MOVE:
                if (childView != null && childView.getMeasuredHeight() <= getScrollY() + getHeight()) {
                    intercepted = false;
                } else if (getScrollY() == 0) {
                    intercepted = false;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (childView != null && childView.getMeasuredHeight() <= getScrollY() + getHeight()) {
                    intercepted = false;
                } else if (getScrollY() == 0) {
                    intercepted = false;
                }
                break;
            default:
                intercepted = super.onInterceptTouchEvent(ev);
                break;
        }
        return intercepted;
    }
}
