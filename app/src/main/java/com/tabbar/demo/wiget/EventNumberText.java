package com.tabbar.demo.wiget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tabbar.demo.R;

/**
 * Created by dick on 2017/9/12.
 */

public class EventNumberText extends RelativeLayout {
    private Drawable src;
    private String name;
    private int number;
    private TextView tv_name;
    private TextView tv_number;
    private ImageView iv_top;

    public EventNumberText(Context context) {
        this(context, null);
    }

    public EventNumberText(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EventNumberText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EventNumberText);
            if (a != null) {
                src = a.getDrawable(R.styleable.EventNumberText_src);
                name = a.getString(R.styleable.EventNumberText_name);
                number = a.getInt(R.styleable.EventNumberText_number, 0);
                a.recycle();
            }
        }
        View view = LayoutInflater.from(context).inflate(R.layout.include_map_event_text, this);
        tv_name = (TextView) view.findViewById(R.id.tv_name);
        tv_number = (TextView) view.findViewById(R.id.tv_number);
        iv_top = (ImageView) view.findViewById(R.id.iv_top);
        if (src != null) {
            iv_top.setImageDrawable(src);
        }
        tv_name.setText(name);
        this.setNumber("" + number);
//        addView(view);
    }

    public Drawable getSrc() {
        return src;
    }

    public void setSrc(Drawable src) {
        this.src = src;
    }

    public void setImageResource(@DrawableRes int resource) {
        iv_top.setImageResource(resource);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return String.valueOf(number);
    }

    public void setNumber(String number) {
        int tempNumber = 0;
        tv_number.setText(number);
        if (!TextUtils.isEmpty(number)) {
            try {
                tempNumber = Integer.parseInt(number);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } finally {
                if (tempNumber > 0) {
                    tv_number.setBackgroundResource(R.drawable.bg_map_event_number);
                } else {
                    tv_number.setBackgroundResource(R.drawable.bg_map_event_number_gray);
                }
                this.number = tempNumber;
            }
        } else {
            tv_number.setBackgroundResource(R.drawable.bg_map_event_number_gray);
        }
    }
}
