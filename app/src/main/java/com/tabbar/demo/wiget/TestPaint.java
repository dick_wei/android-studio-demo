package com.tabbar.demo.wiget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by dick on 2017/10/16.
 */

public class TestPaint extends View {
    private Paint p;
    private int color_red= Color.RED;

    public TestPaint(Context context) {
       this(context,null);
    }

    public TestPaint(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TestPaint(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        p =new Paint();
        p.setAntiAlias(true);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText("画线及弧线：", 10, 60, p);

        //1
        RectF oval2 = new RectF(60, 100, 200, 240);// 设置个新的长方形，扫描测量
        canvas.drawArc(oval2, 200, 130, true, p);
        //2
        RectF rectF=new RectF(100,200,500,600);
        Shader mShader = new LinearGradient(0, 0, 100, 100,
                new int[] { Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW,
                        Color.LTGRAY }, null, Shader.TileMode.REPEAT); // 一个材质,打造出一个线性梯度沿著一条线。
        p.setColor(Color.GREEN);
        p.setShader(mShader);
        canvas.drawArc(rectF,200, 130,true,p);
        canvas.drawText("画贝塞尔曲线:", 100, 450, p);
        Path path=new Path();
        path.moveTo(100,500);
        path.quadTo(150,650,100,800);
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.RED);
        p.setShader(null);
        canvas.drawPath(path,p);
    }
}
