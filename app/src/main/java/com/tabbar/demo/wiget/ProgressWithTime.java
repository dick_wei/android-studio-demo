package com.tabbar.demo.wiget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.blankj.utilcode.util.SizeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by dick on 2017/9/4.
 */

public class ProgressWithTime extends View {
    private Paint pbPaint;
    private Paint textPaint;
    private Context mContext;
    private int colorTxt = Color.parseColor("#ff4444");
    private int colorPb = Color.parseColor("#ff4444");
    private int marginHoricaontal = 40;
    private int marginBottom = 5;
    private float progress = 0;
    private float stepWidth = 0.0f;
    private int lineHeight = 20;
    private float radius = lineHeight / 2.0f;
    private int pointRadius = lineHeight;
    private int textSize = 30;

    public ProgressWithTime(Context context) {
        super(context);
        this.mContext = context;
        this.init();
    }


    public ProgressWithTime(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int speSize = MeasureSpec.getSize(heightMeasureSpec);
        int speMode = MeasureSpec.getMode(heightMeasureSpec);
        int height = 0;
        if (speMode == MeasureSpec.EXACTLY) {
            height = speSize;
        } else {
            height = SizeUtils.dp2px(40);
        }
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), height);
    }

    public ProgressWithTime(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        this.init();
    }

    private void init() {
        pbPaint = new Paint();
        pbPaint.setColor(colorPb);
        pbPaint.setAntiAlias(true);
        pbPaint.setStyle(Paint.Style.FILL);
        textPaint = new Paint();
        textPaint.setColor(colorTxt);
        textPaint.setAntiAlias(true);
        textSize = SizeUtils.dp2px(20);
        marginHoricaontal = SizeUtils.dp2px(40);
        anim();
    }

    private void anim() {
        final Random random = new Random();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progress <= 100) {
                    if (progress > 100) {
                        break;
                    }
                    postInvalidate();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progress += 1;
                }
            }
        }).start();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        stepWidth = (width - marginHoricaontal * 2) / 100.0f;
//        drawBg(canvas, width, height);
        drawProgress(canvas, width, height);
        drawText(canvas, width, height);
    }

    private void drawText(Canvas canvas, int width, int height) {
        float start = stepWidth * progress + marginHoricaontal;
        canvas.drawCircle(start, height - lineHeight - marginBottom, pointRadius, textPaint);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        String time = sdf.format(new Date());
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.measureText(time);
        Rect bounds = new Rect();
        textPaint.getTextBounds(time, 0, time.length(), bounds);
        float txtStart = 0;
        if (txtStart >= marginHoricaontal) {
            txtStart = marginHoricaontal;
        }
        if (progress != 0) {
            canvas.drawText(time, start, height - lineHeight - pointRadius - bounds.height() / 3, textPaint);
        } else {
            canvas.drawText(time, start, height - lineHeight - pointRadius - bounds.height() / 3, textPaint);
        }
    }

    private void drawProgress(Canvas canvas, int width, int height) {
        pbPaint.setColor(colorPb);
        if (progress > 100) {
            progress = 100;
        }
        if (progress > 0) {
            RectF rectF = new RectF(marginHoricaontal + stepWidth * progress, height - lineHeight - marginBottom, width - marginHoricaontal, height - marginBottom);
            canvas.drawRoundRect(rectF, radius, radius, pbPaint);
        }
    }

    public void setProgress(float progress) {
        this.progress = progress;
        invalidate();
    }
}
