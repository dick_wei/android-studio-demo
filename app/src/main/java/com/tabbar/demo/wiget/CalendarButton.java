package com.tabbar.demo.wiget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tabbar.demo.R;

import java.util.Date;

/**
 * Created by dick on 2017/10/20.
 */


class CalendarButton extends LinearLayout {

    Context context;
    TextView tv_date;
    TextView tv_data;
    ImageView iv_note;
    LinearLayout ll_container;
    View view;
    Date date;

    public CalendarButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();
    }

    public CalendarButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();
    }

    public void setTotalEnable(boolean b){
        this.setEnabled(b);
    }

    public CalendarButton(Context context) {
        super(context);

        this.context = context;
        // TODO Auto-generated constructor stub

        init();

    }

    public void setDate(Date date){
        this.date = date;
    }

    public Date getDate(){
        return this.date;
    }

    private void init(){
        view = LayoutInflater.from(context).inflate(R.layout.my_calendar_button, this);

        //findViewById
        tv_date =(TextView)view.findViewById(R.id.textView1);
        tv_data = (TextView)view.findViewById(R.id.textView2);
        iv_note = (ImageView)view.findViewById(R.id.imageView1);
        ll_container = (LinearLayout)view.findViewById(R.id.linearLayout01);

    }

    public void setDateText(String text){
        tv_date.setText(text);
    }

    public void setDateTextColor(int color){
        tv_date.setTextColor(color);
    }
}


