package com.tabbar.demo.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

public class TestAnimActivity extends AppCompatActivity implements View.OnClickListener {

    private Button bt_scale;
    private TextView tv_text;
    private Animation scaleAnim;
    private Button bt_set;
    private Context mContext;
    private Button bt_value1;
    private Button bt_property;
    private Button bt_value_anim;
    private Button bt_anim_set;
    private Button bt_anim_set_all;
    private Button bt_anim_set_seq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim_test);
        mContext = TestAnimActivity.this;
        initView();
        scaleAnim = AnimationUtils.loadAnimation(this, R.anim.scale_1);
    }

    private void initView() {
        bt_scale = (Button) findViewById(R.id.bt_scale);
        tv_text = (TextView) findViewById(R.id.tv_text);
        bt_scale.setOnClickListener(this);
        bt_set = (Button) findViewById(R.id.bt_set);
        bt_set.setOnClickListener(this);
        bt_value1 = (Button) findViewById(R.id.bt_property);
        bt_value1.setOnClickListener(this);
        bt_property = (Button) findViewById(R.id.bt_property);
        bt_property.setOnClickListener(this);
        bt_value_anim = (Button) findViewById(R.id.bt_value_anim);
        bt_value_anim.setOnClickListener(this);
        bt_anim_set = (Button) findViewById(R.id.bt_anim_set);
        bt_anim_set.setOnClickListener(this);
        bt_anim_set_all = (Button) findViewById(R.id.bt_anim_set_all);
        bt_anim_set_all.setOnClickListener(this);
        bt_anim_set_seq = (Button) findViewById(R.id.bt_anim_set_seq);
        bt_anim_set_seq.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_scale:
                tv_text.startAnimation(scaleAnim);
                break;
            case R.id.bt_set:
                tv_text.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.set_1));
                break;
            case R.id.bt_property:
                PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat("translationX", 300);
                PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat("scaleX", 1.0f, 0.1f);
                PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat("scaleY", 1.0f, 0.1f);
                PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat("alpha", 1.0f, 0.1f);
                ObjectAnimator.ofPropertyValuesHolder(bt_value1, p1, p2, p3).setDuration(2000).start();
                break;
            case R.id.bt_value_anim:
                ValueAnimator animator = ValueAnimator.ofFloat(0, 100);
                animator.setDuration(2000);
                animator.setTarget(bt_value_anim);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        ToolLog.w("value", "value:" + animation.getAnimatedValue());
                    }
                });
                animator.start();
                break;
            case R.id.bt_anim_set:
                break;
            case R.id.bt_anim_set_all:
                ObjectAnimator o1 = ObjectAnimator.ofFloat(bt_anim_set_all, "scaleX", 1.0f, 0.1f);
                ObjectAnimator o2 = ObjectAnimator.ofFloat(bt_anim_set_all, "scaleY", 1.0f, 0.1f);
                ObjectAnimator o3 = ObjectAnimator.ofFloat(bt_anim_set_all, "alpha", 1.0f, 0.1f);
                AnimatorSet set1=new AnimatorSet();
                set1.setDuration(2000);
                set1.playTogether(o1,o2,o3);
                set1.start();
                break;
            case R.id.bt_anim_set_seq:
                ObjectAnimator o4 = ObjectAnimator.ofFloat(bt_anim_set_seq, "scaleX", 1.0f, 0.1f);
                ObjectAnimator o5 = ObjectAnimator.ofFloat(bt_anim_set_seq, "scaleY", 1.0f, 0.1f);
                ObjectAnimator o6 = ObjectAnimator.ofFloat(bt_anim_set_seq, "alpha", 1.0f, 0.1f);
                AnimatorSet set2=new AnimatorSet();
                set2.setDuration(1000);
                set2.playSequentially(o4,o5,o6);
                set2.start();
                break;
        }
    }
}
