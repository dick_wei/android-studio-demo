package com.tabbar.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tabbar.demo.R;
import com.tabbar.demo.wiget.FloatScrollView;
import com.tabbar.demo.wiget.MyListView;

import java.util.ArrayList;
import java.util.List;

public class TestSmoothActivity extends AppCompatActivity implements FloatScrollView.OnScrollListener {

    private TextView tv_title;
    private MyListView listview;
    private List<String> dataList = new ArrayList<>();
    private ImageView iv_icon;
    private final String TAG = "title";

    private FloatScrollView myScrollView;
    //悬浮组件距顶部的距离
    private int topHeight;

    private LinearLayout search01, search02;
    //用于获取顶部标题栏layout
    private RelativeLayout rlayout;
    private boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_smooth);
        initView();
        //初始化控件
        initView();
        initData();
    }

    private void initData() {
        for (int i = 0; i < 15; i++) {
            dataList.add("item" + i);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dataList);
        listview.setAdapter(adapter);
    }

    private void initView() {
        myScrollView = (FloatScrollView) findViewById(R.id.myScrollView);
        search01 = (LinearLayout) findViewById(R.id.ll_float_fix);
        search02 = (LinearLayout) findViewById(R.id.ll_float_container);
        rlayout = (RelativeLayout) findViewById(R.id.rl_top_banner);
        myScrollView.setOnScrollListener(this);
        listview = (MyListView) findViewById(R.id.listview);
        tv_title = (TextView) findViewById(R.id.tv_title);
        iv_icon = (ImageView) findViewById(R.id.iv_icon);
        myScrollView.post(new Runnable() {
            @Override
            public void run() {
                myScrollView.scrollTo(0, 0);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            topHeight = rlayout.getBottom();//获取searchLayout的顶部位置
        }
    }

    //监听滚动Y值变化，通过addView和removeView来实现悬停效果
    @Override
    public void onScroll(int y, int oldY) {
        Log.w(TAG, "Y:" + y + "\toldy:" + y + "\t topHeight:" + topHeight);
        if (isFirst) {
            isFirst = false;
            return;
        }
        if (topHeight != 0) {
            if (y >= topHeight) {
                if (tv_title.getParent() != search01) {
                    search02.removeView(tv_title);
                    search01.addView(tv_title);
                }
            } else {
                if (tv_title.getParent() != search02) {
                    search01.removeView(tv_title);
                    search02.addView(tv_title);
                }
                //设置渐变
                float alpha = 1 - ((float) y) / topHeight;
                iv_icon.setAlpha(alpha);
            }
        }
    }
}
