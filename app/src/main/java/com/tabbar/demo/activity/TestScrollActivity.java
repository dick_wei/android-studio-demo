package com.tabbar.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tabbar.demo.R;

public class TestScrollActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_top;
    private TextView tv_mid;
    private TextView tv_bottom;
    private LinearLayout ll_mid;
    private Button bt_move_view;
    private Button bt_move_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scroll);
        initView();
    }

    private void initView() {
        tv_top = (TextView) findViewById(R.id.tv_top);
        tv_mid = (TextView) findViewById(R.id.tv_mid);
        tv_bottom = (TextView) findViewById(R.id.tv_bottom);
        ll_mid = (LinearLayout) findViewById(R.id.ll_mid);
        bt_move_view = (Button) findViewById(R.id.bt_move_view);
        bt_move_view.setOnClickListener(this);
        bt_move_container = (Button) findViewById(R.id.bt_move_container);
        bt_move_container.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_move_container:
                ll_mid.scrollTo(0,250);
                break;
            case R.id.bt_move_view:
                tv_mid.scrollTo(150,600);
                break;
        }
    }
}
