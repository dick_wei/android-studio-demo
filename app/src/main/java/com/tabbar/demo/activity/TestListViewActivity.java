package com.tabbar.demo.activity;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.tabbar.demo.R;
import com.tabbar.demo.wiget.MyListView;

import java.util.ArrayList;
import java.util.List;

public class TestListViewActivity extends AppCompatActivity {
    private LinearLayout titlebar;
    private MyListView listview;
    private float mFirstY;
    private float mCurrentY;
    protected float mTouchSlop;
    private ObjectAnimator mAnimatorTitle;
    private ObjectAnimator mAnimatorContent;
    private ArrayAdapter adapter;
    private List<String> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_list_view);
        initView();
        initData();
        setView();
    }


    private void initView() {
        listview = (MyListView) findViewById(R.id.listview);
        titlebar = (LinearLayout) findViewById(R.id.ll_top_titlebar);
        mTouchSlop = ViewConfiguration.get(this).getScaledTouchSlop();
    }


    private void initData() {
        for (int i = 0; i < 15; i++) {
            dataList.add("item" + i);
        }
        Log.w("list", "mTouchSlop:" + mTouchSlop);
    }

    private void setView() {
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, dataList);
        listview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mFirstY=event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        mCurrentY=event.getY();
                        Log.w("list","mTouchSlop:"+mTouchSlop+"\tmFirstY:"+mFirstY+"\t mCurrentY:"+mCurrentY);
                        if(mCurrentY-mFirstY>mTouchSlop){
                            showHideTitlebar(true);
                        }else if(mCurrentY-mFirstY<mTouchSlop){
                            showHideTitlebar(false);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return false;
            }
        });

        listview.setAdapter(adapter);
    }

    protected void showHideTitlebar(boolean tag) {
        if (mAnimatorTitle != null && mAnimatorTitle.isRunning()) {
            mAnimatorTitle.cancel();
        }
        if (mAnimatorContent != null && mAnimatorContent.isRunning()) {
            mAnimatorContent.cancel();
        }
        if (tag) {
            mAnimatorTitle = ObjectAnimator.ofFloat(titlebar, "translationY", titlebar.getTranslationY(), 0);
            float y = listview.getTranslationY();
            int height = titlebar.getHeight();
            mAnimatorContent = ObjectAnimator.ofFloat(listview, "translationY", listview.getTranslationY(), height);
            float theY = listview.getY();
            System.out.println(theY);
        } else {
            mAnimatorTitle = ObjectAnimator.ofFloat(titlebar, "translationY", titlebar.getTranslationY(), -titlebar.getHeight());
            mAnimatorContent = ObjectAnimator.ofFloat(listview, "translationY", listview.getTranslationY(), 0);
            float theY = listview.getY();
            System.out.println(theY);
        }
        mAnimatorTitle.start();
        mAnimatorContent.start();
    }
}
