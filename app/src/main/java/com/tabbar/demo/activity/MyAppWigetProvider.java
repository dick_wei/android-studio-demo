package com.tabbar.demo.activity;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.blankj.utilcode.util.ToastUtils;
import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

/**
 * Created by wq on 2017/12/15.
 */

public class MyAppWigetProvider extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        int wigetId = appWidgetIds[0];
        Intent intent = new Intent(context, TestWatchActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        RemoteViews remoteView = new RemoteViews(context.getPackageName(), R.layout.wiget_layout);
        remoteView.setOnClickPendingIntent(R.id.imageView, pendingIntent);
        appWidgetManager.updateAppWidget(wigetId, remoteView);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        ToolLog.w("wiget", "***********onReceive***");
        ToastUtils.showShort("onreceive");
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        ToastUtils.showShort("************onReceive***********");
    }
}
