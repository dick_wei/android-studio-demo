package com.tabbar.demo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.tabbar.demo.R;

public class TestShareActivity extends AppCompatActivity implements View.OnClickListener {

    private Button bt_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_share);
        initView();
    }

    private void shareContent() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        String title = "标题";
        String extraText = "给大家介绍一个好网站，https://www.baidu.com";
        share.putExtra(Intent.EXTRA_TEXT, extraText);
        if (title != null) {
            share.putExtra(Intent.EXTRA_SUBJECT, title);
        }
        startActivity(Intent.createChooser(share, "分享一下"));
    }

    private void initView() {
        bt_share = (Button) findViewById(R.id.bt_share);

        bt_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_share:
                shareContent();
                break;
        }
    }
}
