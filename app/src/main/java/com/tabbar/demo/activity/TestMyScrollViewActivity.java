package com.tabbar.demo.activity;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ListView;

import com.blankj.utilcode.util.ImageUtils;
import com.tabbar.demo.R;

public class TestMyScrollViewActivity extends AppCompatActivity {
    ImageView iv_bottom;

    /**
     * @floatRange(
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_my_scroll_view);
        BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(R.drawable.fengjing);
        Bitmap bitmap = drawable.getBitmap();
        Bitmap blurBitmap = ImageUtils.fastBlur(bitmap, 0.5f, 10);
        iv_bottom= (ImageView) findViewById(R.id.iv_bottom);
        iv_bottom.setImageBitmap(blurBitmap);
        ListView listView;
    }

    /**
     *
     * @param id
     */
    private void getImg(@IntRange(from = 0 ,to = 1000 ) int id){
    }
}
