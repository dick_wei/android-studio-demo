package com.tabbar.demo.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;
import com.tabbar.demo.wiget.MySwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

public class TestSwipeRefreshActivity extends AppCompatActivity {

    private ListView listview;
    private List<String> dataList = new ArrayList<>();
    private MySwipeRefreshLayout refresh_layout;
    private int lastItem = 0;
    private int totalItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_swipe_refresh);
        initView();
        initData();
        setView();
    }

    private void setView() {
        ArrayAdapter<String> adpater = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataList);
        listview.setAdapter(adpater);
        refresh_layout.setmListView(listview);
        refresh_layout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh_layout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh_layout.setRefreshing(false);
                    }
                }, 3000);
            }
        });
        refresh_layout.setOnLoadListener(new MySwipeRefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                refresh_layout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh_layout.setLoading(false);
                    }
                }, 2500);
            }
        });
        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (totalItem == lastItem && scrollState == SCROLL_STATE_IDLE) {
                    ToolLog.v("ev", " ------>onload..yes");
//                    if (!isLoading) {
//                        isLoading = true;
//                        footer.setVisibility(View.VISIBLE);
//                        onLoadMore.loadMore();//上拉加载
//                    }
                }
                //判断ListView是否滑动到第一个Item的顶部
                if (listview.getChildCount() > 0 && listview.getFirstVisiblePosition() == 0
                        && listview.getChildAt(0).getTop() >= listview.getPaddingTop()) {
                    //解决滑动冲突，当滑动到第一个item，下拉刷新才起作用
                    refresh_layout.setEnabled(true);
                } else {
                    refresh_layout.setEnabled(false);
                }
            }

            @Override
            public void onScroll(AbsListView listview, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                lastItem = firstVisibleItem + visibleItemCount;
                totalItem = totalItemCount;
            }
        });
    }

    private void initData() {
        for (int i = 0; i < 15; i++) {
            dataList.add("item" + i);
        }

    }

    private void initView() {
        listview = (ListView) findViewById(R.id.listview);
        refresh_layout = (MySwipeRefreshLayout) findViewById(R.id.refresh_layout);

    }
}
