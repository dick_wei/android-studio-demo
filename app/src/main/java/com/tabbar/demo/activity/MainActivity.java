package com.tabbar.demo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.tabbar.demo.R;
import com.tabbar.demo.adapter.HomeFragmentAdapter;
import com.tabbar.demo.fragment.ChartViewFragment;
import com.tabbar.demo.fragment.CityFragment;
import com.tabbar.demo.fragment.HomeFragment;
import com.tabbar.demo.fragment.MessageFragment;
import com.tabbar.demo.lib.MainNavigateTabBar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mIvAdd;
    private HomeFragmentAdapter adapter;
    private List<Fragment> fragmentList = new ArrayList<>();
    private String[] titles = new String[]{"首页", "同城", "消息", "我的"};
    private int[] normalImgs = new int[]{R.mipmap.comui_tab_home, R.mipmap.comui_tab_city, R.mipmap.comui_tab_message, R.mipmap.comui_tab_person};


    private MainNavigateTabBar tabBar;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabBar = (MainNavigateTabBar) findViewById(R.id.mainTabBar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mIvAdd = (ImageView) findViewById(R.id.iv_add);
        tabBar.onRestoreInstanceState(savedInstanceState);
        doBusiness(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        tabBar.onSaveInstanceState(outState);
    }


    public void doBusiness(Bundle saveInstance) {
        initFragment();
        adapter = new HomeFragmentAdapter(getSupportFragmentManager(),
                this, titles, fragmentList);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        mIvAdd.setOnClickListener(this);
        // 使用第三方tabbar
        tabBar.onRestoreInstanceState(saveInstance);
        int length = titles.length + 1;
        for (int i = 0; i < length; i++) {
            int position = i;
            if (i > 2) {
                position = i - 1;
            }
            if (i != 2) {
                tabBar.addTab(position, new MainNavigateTabBar.TabParam(normalImgs[position], R.mipmap.ic_refresh, titles[position]));
            } else {
                tabBar.addTab(i, new MainNavigateTabBar.TabParam(0, 0, ""));
            }
        }

        tabBar.setTabSelectListener(new MainNavigateTabBar.OnTabSelectedListener() {
            @Override
            public void onTabSelected(MainNavigateTabBar.ViewHolder holder) {
                int position = holder.tabIndex;
                viewPager.setCurrentItem(position);
            }
        });
        tabBar.setTabItemClickListener(new MainNavigateTabBar.OnTabItemClickListener() {
            @Override
            public void onTabClicked(MainNavigateTabBar.ViewHolder holder) {
                if (holder.tabIndex == tabBar.getCurrentSelectedTab()) {
                    showToast("刷新" + holder.tabTitle.getText());
                }
            }
        });
    }

    private void initFragment() {
        fragmentList.add(CityFragment.newInstance(1));
        fragmentList.add(HomeFragment.newInstance(0));
        fragmentList.add(MessageFragment.newInstance(2));
        fragmentList.add(ChartViewFragment.newInstance(3));
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.w("input","----->dispatchTouchEvent:"+ev.getAction());
//        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
//            View v = getCurrentFocus();
//            if (isShouldHideKeyboard(v, ev)) {
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//            }
//        }
        return super.dispatchTouchEvent(ev);
    }

    // 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add:
                showToast("点击了add");
                MainAddDialog mainAddDialog = new MainAddDialog();
                mainAddDialog.build(MainActivity.this);
                mainAddDialog.showDialog();
                break;
        }
    }

    public void showToast(String text) {
        Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
    }
}
