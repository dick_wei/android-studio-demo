package com.tabbar.demo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tabbar.demo.R;
import com.tabbar.demo.adapter.DividerGridItemDecoration;
import com.tabbar.demo.adapter.HomeAdapter;

import java.util.ArrayList;

public class TestRecycleViewActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView iv_back;
    private TextView tv_title;
    private RecyclerView recycleview;
    private ArrayList<String> mDatas;
    private HomeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_recycle_view);
        initView();
        initData();
        setView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setView() {
        iv_back.setOnClickListener(this);
        //listview
//        recycleview.setLayoutManager(new LinearLayoutManager(TestRecycleViewActivity.this,LinearLayoutManager.VERTICAL,false));
//        mAdapter = new HomeAdapter(TestRecycleViewActivity.this, mDatas);
//        recycleview.setAdapter(mAdapter);
//        recycleview.addItemDecoration(new LinearDecorator(TestRecycleViewActivity.this, LinearDecorator.VERTICAL_LIST));
        //grideView
        recycleview.setLayoutManager(new GridLayoutManager(TestRecycleViewActivity.this,4));
        mAdapter = new HomeAdapter(TestRecycleViewActivity.this, mDatas);
        recycleview.setAdapter(mAdapter);
        recycleview.addItemDecoration(new DividerGridItemDecoration(TestRecycleViewActivity.this));
        recycleview.setItemAnimator(new DefaultItemAnimator());
    }


    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
    }

    private void initData() {
        mDatas = new ArrayList<String>();
        for (int i = 'A'; i < 'z'; i++) {
            mDatas.add("" + (char) i);
        }
    }

    @Override
    public void onClick(View v) {

    }
}
