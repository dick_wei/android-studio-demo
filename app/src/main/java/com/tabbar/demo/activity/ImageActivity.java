package com.tabbar.demo.activity;

import android.app.Activity;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;

import java.util.HashMap;
import java.util.Map;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "ImageActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ImageView imageView = (ImageView) findViewById(R.id.image);
        PhotoViewAttacher attacher = new PhotoViewAttacher(imageView);
        initView();
//        fadeView();
    }


    private void initView() {
        Button btTest = (Button) findViewById(R.id.bt_test);
        btTest.setOnClickListener(this);
        ControllerListener listener = new ControllerListener() {
            @Override
            public void onSubmit(String id, Object callerContext) {
                ToolLog.w(TAG, "------->onSubmit");
            }

            @Override
            public void onFinalImageSet(String id, Object imageInfo, Animatable animatable) {
                ToolLog.w(TAG, "------->onFinalImageSet");
            }

            @Override
            public void onIntermediateImageSet(String id, Object imageInfo) {
                ToolLog.w(TAG, "------->onIntermediateImageSet");
            }

            @Override
            public void onIntermediateImageFailed(String id, Throwable throwable) {
                ToolLog.w(TAG, "------->onIntermediateImageFailed");
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                ToolLog.w(TAG, "------->onFailure");
            }

            @Override
            public void onRelease(String id) {
                ToolLog.w(TAG, "------->onRelease");
            }
        };
        Uri uri = Uri.parse("http://images.csdn.net/20170930/31.jpg");
        //controller
        DraweeController control = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setControllerListener(listener)
                .build();
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.simple_drawee_view);
        draweeView.setController(control);
//        draweeView.setImageURI(uri);
    }


    /**
     * 渐近时加载
     */
    private void fadeView() {
        SimpleDraweeView draweeView = (SimpleDraweeView) findViewById(R.id.simple_drawee_view2);
//        GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(getResources())
//                .setFadeDuration(1500)
//                .setBackground(getResources().getDrawable(R.drawable.ic_creation))
//                .setPlaceholderImage(R.drawable.fengjing)
//                .build();
//        draweeView.setHierarchy(hierarchy);
        Uri uri = Uri.parse("http://images.csdn.net/20170930/31.jpg");
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setProgressiveRenderingEnabled(true)
                .build();
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .build();
        draweeView.setController(controller);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_test:
                new Thread(new TestArrayMap()).start();
                break;
        }
    }

    class TestArrayMap implements Runnable {
        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            Map<Integer, Object> hashMap = new HashMap<>();
            for (int i = 0; i < 500; i++) {
                hashMap.put(i, "curent is " + i);
            }
            ToolLog.w("time", "start hashMap" + (System.currentTimeMillis() - startTime));
            startTime = System.currentTimeMillis();
            ArrayMap<Integer, Object> arrayMap = new ArrayMap<>();
            for (int i = 0; i < 500; i++) {
                arrayMap.put(i, "curent is " + i);
            }
            ToolLog.w("time", "start arrayMap" + (System.currentTimeMillis() - startTime));
            startTime = System.currentTimeMillis();
            for (int i = 100; i < 300; i++) {
                String str = hashMap.get(i).toString();
                System.out.println(str);
            }

            ToolLog.w("time", "start hashMap get" + (System.currentTimeMillis() - startTime));
            startTime = System.currentTimeMillis();
            for (int i = 100; i < 300; i++) {
                String str = hashMap.get(i).toString();
                System.out.println(str);
            }
            ToolLog.w("time", "start arrayMap get" + (System.currentTimeMillis() - startTime));

        }
    }
}
