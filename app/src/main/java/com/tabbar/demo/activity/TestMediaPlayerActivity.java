package com.tabbar.demo.activity;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.tabbar.demo.R;

import java.io.IOException;

/**
 * Created by dick on 2017/11/3.
 */

public class TestMediaPlayerActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnStart;
    private Button btnPause;
    private Button btnRestart;
    private Button btnStop;

    public static final String AUDIO_PATH = "http://www.baidu.mp3/song1.mp3";// 网络音乐
    // Environment.getExternalStorageDirectory() + "/Music/song1.mp3" ; //sdcard
    // 音乐文件
    // Environment.getExternalStorageDirectory() + "/Movies/vedio.mp4";
    private MediaPlayer mMediaPlayer;
    private int currentPosition;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnPause = (Button) findViewById(R.id.btnPause);
        btnRestart = (Button) findViewById(R.id.btnRestart);
        btnStop = (Button) findViewById(R.id.btnStop);

        btnStart.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnRestart.setOnClickListener(this);
        btnStop.setOnClickListener(this);

    }

    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btnStart:
//                playAudio(AUDIO_PATH);//
                playLocalAudio();
                break;
            case R.id.btnPause:
                if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                    currentPosition = mMediaPlayer.getCurrentPosition();
                    mMediaPlayer.pause();
                }
                break;
            case R.id.btnRestart:
                if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
                    mMediaPlayer.seekTo(currentPosition);
                    mMediaPlayer.start();
                }
                break;
            case R.id.btnStop:
                if (mMediaPlayer != null) {
                    mMediaPlayer.stop();
                    currentPosition = 0;
                }
                break;
        }

    }

    private void playAudio(String url) {// 播放url音乐文件

        try {
            killMediaPlayer();// 播放前，先kill原来的mediaPlayer
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void killMediaPlayer() {
        // TODO Auto-generated method stub
        if (null != mMediaPlayer) {
            mMediaPlayer.release();
        }
    }

    private void playLocalAudio() {// 播放本地音乐文件
        mMediaPlayer = MediaPlayer.create(this, R.raw.rescue_kunren);
        mMediaPlayer.start();// 在此情况下，不需要调用prepare()方法
    }

    private void playLocalAudio_UsingDescriptor() {//先打开文件，再将文件给mediaPlayer
        try {
            AssetFileDescriptor fileDesc = getResources().openRawResourceFd(
                    R.raw.rescue_kunren);

            if (null != fileDesc) {
                mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setDataSource(fileDesc.getFileDescriptor(),
                        fileDesc.getStartOffset(), fileDesc.getLength());

                fileDesc.close();
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        killMediaPlayer();//destroy中释放资源
    }
}
