package com.tabbar.demo.activity;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ScreenUtils;
import com.tabbar.demo.R;

/**
 * Created by dick on 2017/9/12.
 */

public class MainAddDialog implements View.OnClickListener {
    private Context context;
    private Dialog dialog;
    private RelativeLayout mRlIvContainer;

    public Dialog build(Context context) {
        this.context=context;
        dialog = new Dialog(context, R.style.ActionSheetDialogStyle);
        dialog.setCanceledOnTouchOutside(true);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_main_add, null);
        mRlIvContainer = (RelativeLayout) view.findViewById(R.id.rl_add_container);
        LinearLayout ll_signin = (LinearLayout) view.findViewById(R.id.ll_signin);
        LinearLayout ll_report = (LinearLayout) view.findViewById(R.id.ll_report_fault);
        LinearLayout ll_trap = (LinearLayout) view.findViewById(R.id.ll_trap_people);
        mRlIvContainer.setOnClickListener(this);
        ll_signin.setOnClickListener(this);
        ll_report.setOnClickListener(this);
        ll_trap.setOnClickListener(this);
        int width = ScreenUtils.getScreenWidth();
        int height = ScreenUtils.getScreenHeight();
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.addContentView(view, lp);
//        startAnim(context);
        return dialog;
    }

    private void startAnim(Context context) {
        final Animation animation = AnimationUtils.loadAnimation(context, R.anim.translate_main_add);
        animation.setInterpolator(new LinearInterpolator());
        LayoutAnimationController controller = new LayoutAnimationController(animation);
        controller.setOrder(LayoutAnimationController.ORDER_NORMAL);
        controller.setInterpolator(new LinearInterpolator());
        mRlIvContainer.setLayoutAnimation(controller);
    }

    public void showDialog() {
        if (dialog != null) {
            dialog.show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.rl_add_container:
                dismiss();
                break;
            case R.id.ll_signin:
                dismiss();
                break;
            case R.id.ll_report_fault:
                dismiss();
                break;
            case R.id.ll_trap_people:
                dismiss();
                break;
        }
    }

    private void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
