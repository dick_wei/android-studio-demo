package com.tabbar.demo.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dick on 2017/9/8.
 */

public class TestParcel implements Parcelable {
    private String name;
    private int age;
    private String title;
    private String old;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.age);
        dest.writeString(this.title);
        dest.writeString(this.old);
    }

    public TestParcel() {
    }

    protected TestParcel(Parcel in) {
        this.name = in.readString();
        this.age = in.readInt();
        this.title = in.readString();
        this.old = in.readString();
    }

    public static final Parcelable.Creator<TestParcel> CREATOR = new Parcelable.Creator<TestParcel>() {
        @Override
        public TestParcel createFromParcel(Parcel source) {
            return new TestParcel(source);
        }

        @Override
        public TestParcel[] newArray(int size) {
            return new TestParcel[size];
        }
    };
}
