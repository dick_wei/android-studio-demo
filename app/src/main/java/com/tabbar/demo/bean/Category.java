package com.tabbar.demo.bean;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private String mCategoryName;
    private List<CategoryItem> mCategoryItem = new ArrayList<>();

    public Category(String mCategroyName) {
        mCategoryName = mCategroyName;
    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public void addItem(CategoryItem pItemName) {
        mCategoryItem.add(pItemName);
    }

    public void addItem(String pItemName) {
        mCategoryItem.add(new CategoryItem(pItemName));
    }

    /**
     * 获取Item内容
     *
     * @param pPosition
     * @return
     */
    public CategoryItem getItem(int pPosition) {
        // Category排在第一位  
        if (pPosition == 0) {
            return new CategoryItem(mCategoryName);
        } else {
            return mCategoryItem.get(pPosition - 1);
        }
    }

    /**
     * 当前类别Item总数。Category也需要占用一个Item
     *
     * @return
     */
    public int getItemCount() {
        return mCategoryItem.size() + 1;
    }

}  