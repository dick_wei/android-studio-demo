package com.tabbar.demo.bean;

/**
 * Created by dick on 2017/8/24.
 */

public class CategoryItem {
    private String name;
    private Class aClass;

    public CategoryItem(String name) {
        this.name = name;
    }

    public CategoryItem(String name, Class aClass) {
        this.name = name;
        this.aClass = aClass;
    }

    public CategoryItem() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getaClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }
}
