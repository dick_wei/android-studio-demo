package com.tabbar.demo.bean;

import java.util.List;

/**
 * Created by dick on 2017/9/6.
 */

public class TestGsonFormat {

    /**
     * message :
     * result : {"elevatorList":[{"name":"天门山1单元1梯","elevatorId":1036},{"name":"天门山1单元2梯","elevatorId":1037},{"name":"天门山1单元3梯","elevatorId":1038},{"name":"天门山1单元4梯","elevatorId":1039},{"name":"天门山1单元5梯","elevatorId":1040},{"name":"天门山1单元6梯","elevatorId":1041},{"name":"天门山1单元7梯","elevatorId":1042},{"name":"天门山1单元8梯","elevatorId":1043},{"name":"天门山1单元9梯","elevatorId":1044},{"name":"天门山1单元10梯","elevatorId":1045}]}
     * hasNext : true
     * extra : ["南天门","天门山"]
     * code :
     * success : true
     */

    private String message;
    private ResultBean result;
    private boolean hasNext;
    private String code;
    private boolean success;
    private List<String> extra;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<String> getExtra() {
        return extra;
    }

    public void setExtra(List<String> extra) {
        this.extra = extra;
    }

    public static class ResultBean {
        private List<ElevatorListBean> elevatorList;

        public List<ElevatorListBean> getElevatorList() {
            return elevatorList;
        }

        public void setElevatorList(List<ElevatorListBean> elevatorList) {
            this.elevatorList = elevatorList;
        }

        public static class ElevatorListBean {
            /**
             * name : 天门山1单元1梯
             * elevatorId : 1036
             */

            private String name;
            private int elevatorId;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getElevatorId() {
                return elevatorId;
            }

            public void setElevatorId(int elevatorId) {
                this.elevatorId = elevatorId;
            }
        }
    }
}
