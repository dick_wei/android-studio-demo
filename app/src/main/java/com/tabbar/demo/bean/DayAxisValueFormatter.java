package com.tabbar.demo.bean;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by philipp on 02/06/16.
 */
public class DayAxisValueFormatter implements IAxisValueFormatter {

    private int xAxisLen;

    private LineChart chart;

    public DayAxisValueFormatter(LineChart chart, int xAxisLength) {
        this.chart = chart;
        this.xAxisLen = xAxisLength;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return getDay((int) value);
    }

    private String getDay(int value) {
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - (this.xAxisLen - value));
        String dayAfter = new SimpleDateFormat("dd").format(c.getTime());
        if (value == this.xAxisLen) {
            dayAfter += "日";
        }
        return dayAfter;
    }

}
