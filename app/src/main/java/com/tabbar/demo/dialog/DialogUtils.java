package com.tabbar.demo.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.util.Utils;
import com.tabbar.demo.R;
import com.tabbar.demo.util.ToolLog;
import com.tabbar.demo.wiget.ResizeLayout;

/**
 * Created by dick on 2017/9/22.
 */

public class DialogUtils {
    private static DialogUtils instance;
    private AlertDialog dialog;
    private EditText mEditText;
    private TextView mTvSend;
    private boolean isShowInput = false;

    private DialogUtils() {
    }

    public static DialogUtils getInstance() {
        if (instance == null) {
            synchronized (DialogUtils.class) {
                if (instance == null) {
                    instance = new DialogUtils();
                }
            }
        }
        return instance;
    }

    public void showDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Dialog_Fullscreen);
        final View view = LayoutInflater.from(activity).inflate(R.layout.dialog_edittext, null);
        builder.setView(view);
        builder.setCancelable(true);    //设置按钮是否可以按返回键取消,false则不可以取消
        //创建对话框
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true); //设置弹出框失去焦点是否隐藏,即点击屏蔽其它地方是否隐藏
        final Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        addListener(view);
        dialog.show();
        //获取焦点
//        //设置大小
        setDialog(activity);
    }

    private void setDialog(final Activity activity) {
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.x = 0;
        lp.y = 0;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        final int height = (int) activity.getResources().getDimension(R.dimen.parent_list_height);
        lp.height = height;
        dialog.getWindow().setAttributes(lp);
        mEditText = (EditText) dialog.getWindow().findViewById(R.id.edt_text);
        mTvSend = (TextView) dialog.getWindow().findViewById(R.id.tv_send);
        mEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                ToolLog.w("input", "--------->keyCode:" + keyCode);
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    dialog.dismiss();
                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss();
                }
                return false;
            }
        });
        mEditText.postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyBoardUtils.openKeybord(mEditText, activity);
            }
        }, 100);

        //自动弹出输入法框
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface dg) {
                //调用系统输入法
                if (isShowInput) {
                    toggleSoftInput();
                }
            }
        });
        mTvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToolLog.w("input","点击了发送");
            }
        });
    }

    private void addListener(View view) {
        final ResizeLayout rootLayout = (ResizeLayout) view.findViewById(R.id.resize_layout);
        rootLayout.setListener(new ResizeLayout.Listener() {
            @Override
            public void onSoftKeyboardShown(boolean isShowing) {
                ToolLog.w("input", "--------->isShowingL:" + isShowing);
                isShowInput = isShowing;
            }
        });
    }

    /**
     * 动态隐藏软键盘
     *
     * @param context 上下文
     * @param view    视图
     */
    public static void hideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 切换键盘显示与否状态
     */
    public void toggleSoftInput() {
        InputMethodManager imm = (InputMethodManager) Utils.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm == null) return;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
