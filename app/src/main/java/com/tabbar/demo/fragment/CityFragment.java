package com.tabbar.demo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tabbar.demo.R;
import com.tabbar.demo.activity.ImageActivity;
import com.tabbar.demo.activity.LineChartActivity2;
import com.tabbar.demo.activity.TestAnimActivity;
import com.tabbar.demo.activity.TestChartActivity;
import com.tabbar.demo.activity.TestListViewActivity;
import com.tabbar.demo.activity.TestMediaPlayerActivity;
import com.tabbar.demo.activity.TestMyScrollViewActivity;
import com.tabbar.demo.activity.TestPaintActivity;
import com.tabbar.demo.activity.TestRecycleViewActivity;
import com.tabbar.demo.activity.TestRxJavaActivity;
import com.tabbar.demo.activity.TestScrollActivity;
import com.tabbar.demo.activity.TestShareActivity;
import com.tabbar.demo.activity.TestSmoothActivity;
import com.tabbar.demo.activity.TestSwipeRefreshActivity;
import com.tabbar.demo.activity.TestViewDraghelperActivity;
import com.tabbar.demo.activity.TestWatchActivity;
import com.tabbar.demo.adapter.CategoryAdapter;
import com.tabbar.demo.bean.Category;
import com.tabbar.demo.bean.CategoryItem;
import com.tabbar.demo.dialog.DialogUtils;
import com.tabbar.demo.service.ForegroundEnablingService;

import java.util.ArrayList;

/**
 * User:Shine
 * Date:2015-10-20
 * Description:
 */
public class CityFragment extends PageFragment {

    private CategoryAdapter mCustomBaseAdapter;

    public static PageFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARGS_PAGE, page);
        CityFragment fragment = new CityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city, container, false);
        setValue(view);
        return view;
    }

    private void initView() {

    }

    private void setValue(View view) {

        ListView listView = (ListView) view.findViewById(R.id.listView1);

        // 数据
        ArrayList<Category> listData = getData();

        mCustomBaseAdapter = new CategoryAdapter(getActivity(), listData);

        // 适配器与ListView绑定
        listView.setAdapter(mCustomBaseAdapter);

        listView.setOnItemClickListener(new ItemClickListener());
    }


    private class ItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            CategoryItem item = mCustomBaseAdapter.getItem(position);
            showShort(item.getName());
            if (item.getaClass() != null && item.getaClass().getSimpleName().contains("Activity")) {
                startActivity(new Intent(getActivity(), item.getaClass()));
            } else if (item.getaClass() != null && item.getaClass().getSimpleName().contains("Service")) {
                getActivity().startService(new Intent(getActivity(), item.getaClass()));
            } else {
                if ("dialog test".equalsIgnoreCase(item.getName())) {
                    DialogUtils.getInstance().showDialog(getActivity());
                } else {
                    showShort(item.getName());
                }
            }
        }
    }

    /**
     * 创建测试数据
     */
    private ArrayList<Category> getData() {
        ArrayList<Category> listData = new ArrayList<Category>();
        Category categoryOne = new Category("路人甲");
        categoryOne.addItem(new CategoryItem("scrollview test", TestMyScrollViewActivity.class));
        categoryOne.addItem(new CategoryItem("rxjava test", TestRxJavaActivity.class));
        categoryOne.addItem(new CategoryItem("listview test", TestListViewActivity.class));
        categoryOne.addItem(new CategoryItem("test play mp3", TestMediaPlayerActivity.class));
        categoryOne.addItem(new CategoryItem("test 分享", TestShareActivity.class));
        categoryOne.addItem(new CategoryItem("test view scroll", TestScrollActivity.class));
        categoryOne.addItem(new CategoryItem("viewdraghelper test", TestViewDraghelperActivity.class));
        categoryOne.addItem(new CategoryItem("watch View test", TestWatchActivity.class));
        categoryOne.addItem(new CategoryItem("piechart test", TestChartActivity.class));
        categoryOne.addItem(new CategoryItem("linechart2 test", LineChartActivity2.class));
        categoryOne.addItem(new CategoryItem("forgetService test", ForegroundEnablingService.class));
        categoryOne.addItem(new CategoryItem("recycleview test", TestRecycleViewActivity.class));
        categoryOne.addItem(new CategoryItem("smooth scrollview test", TestSmoothActivity.class));
        categoryOne.addItem(new CategoryItem("swipeRefreshLayout  test", TestSwipeRefreshActivity.class));
        categoryOne.addItem(new CategoryItem("imageActivity test", ImageActivity.class));
        categoryOne.addItem(new CategoryItem("paint test", TestPaintActivity.class));
        categoryOne.addItem(new CategoryItem("anim test", TestAnimActivity.class));
        categoryOne.addItem(new CategoryItem("dialog test"));
        categoryOne.addItem("赵本山");
        categoryOne.addItem("郭德纲");
        categoryOne.addItem("周立波");
        Category categoryTwo = new Category("事件乙");
        categoryTwo.addItem("**贪污");
        categoryTwo.addItem("**照门");
        Category categoryThree = new Category("书籍丙");
        categoryThree.addItem("10天学会***");
        categoryThree.addItem("**大全");
        categoryThree.addItem("**秘籍");
        categoryThree.addItem("**宝典");
        categoryThree.addItem("10天学会***");
        categoryThree.addItem("10天学会***");
        categoryThree.addItem("10天学会***");
        categoryThree.addItem("10天学会***");
        Category categoryFour = new Category("书籍丙");
        categoryFour.addItem("河南");
        categoryFour.addItem("天津");
        categoryFour.addItem("北京");
        categoryFour.addItem("上海");
        categoryFour.addItem("广州");
        categoryFour.addItem("湖北");
        categoryFour.addItem("重庆");
        categoryFour.addItem("山东");
        categoryFour.addItem("陕西");

        listData.add(categoryOne);
        listData.add(categoryTwo);
        listData.add(categoryThree);
        listData.add(categoryFour);

        return listData;
    }

}
