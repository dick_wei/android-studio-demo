package com.tabbar.demo.fragment;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.tabbar.demo.R;
import com.tabbar.demo.bean.DayAxisValueFormatter;
import com.tabbar.demo.util.YAxisValueFormatter;
import com.tabbar.demo.wiget.MyMarkerView;

import java.util.ArrayList;


public class ChartViewFragment extends Fragment {
    public static final String ARGS_PAGE = "args_page";
    private int mPage;
    private LineChart mChart;
    private int DAYS = 6;
    private int mFillColor = Color.argb(150, 51, 181, 229);

    public static ChartViewFragment newInstance(int page) {
        Bundle args = new Bundle();

        args.putInt(ARGS_PAGE, page);
        ChartViewFragment fragment = new ChartViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARGS_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart, container, false);
        initView(view);
        ArrayList<Entry> entries = initData(5, 10);
        setDataSet(entries);
        setAnim();
        mChart.invalidate();
        return view;
    }

    private void setAnim() {
        mChart.animateX(500);
    }

    private void initView(View view) {
        mChart = (LineChart) view.findViewById(R.id.linechart);
        setChart();
        setXYLines();

    }

    public void showShort(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }


    private void setXYLines() {
        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(DAYS, false);
        IAxisValueFormatter dayAxisValueFormatter = new DayAxisValueFormatter(mChart, DAYS);
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setValueFormatter(dayAxisValueFormatter);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setAxisMaximum(1000000);
        leftAxis.setAxisMinimum(0);
//        leftAxis.setLabelCount(5, true);
//        leftAxis.setDrawAxisLine(false);
//        leftAxis.setDrawZeroLine(false);
//        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawGridLines(false);
        leftAxis.setEnabled(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setValueFormatter(new YAxisValueFormatter());
        mChart.getAxisRight().setEnabled(false);
    }

    private void setChart() {
        mChart.setBackgroundColor(Color.WHITE);
        mChart.setGridBackgroundColor(Color.WHITE);
        mChart.setDrawGridBackground(true);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);
        mChart.setTouchEnabled(true);
        //隐藏边框
        mChart.setDrawBorders(false);
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view2);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart
        // no description text
        mChart.getDescription().setEnabled(false);

        //设置单方向和双方向缩放 true x,y方向可以同时控制，false只能控制x方向的缩小放大或者Y方向的缩小放大
        mChart.setPinchZoom(false);
        Legend l = mChart.getLegend();
        l.setEnabled(false);
    }

    private ArrayList<Entry> initData(int count, float range) {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        for (int i = 0; i < count; i++) {
            int multi= (int) (range/2);
            float val = (float) (Math.random() * multi)+5;// + (float)
            yVals1.add(new Entry(i, val));
        }
        return yVals1;
    }

    private void setDataSet(ArrayList<Entry> yVals1) {
        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(yVals1, "DataSet 1");
            set1.setMode(LineDataSet.Mode.LINEAR);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            set1.setColor(getResources().getColor(R.color.color_main_blue));
            set1.setDrawCircles(true);
            set1.setLineWidth(2f);
            set1.setCircleRadius(3f);
            set1.setFillAlpha(255);
            set1.setDrawFilled(false);
            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.fade_green);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(mFillColor);
            }
            set1.setHighLightColor(Color.TRANSPARENT);//设置高亮色
            set1.setDrawCircleHole(false);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mChart.getAxisLeft().getAxisMinimum();
                }
            });


            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets
            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            data.setDrawValues(true);
            // set data
            mChart.setData(data);
        }
    }
}