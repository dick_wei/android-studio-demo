package com.tabbar.demo.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.tabbar.demo.R;
import com.tabbar.demo.activity.ImageActivity;

import java.util.List;

/**
 * Created by dick on 2017/9/11.
 */

public class NotifacationManager {
    private static volatile NotifacationManager instance;
    private static final int CLICK_CODE = 0x11;
    private static final int DISMISS_CODE = 0x13;

    private NotifacationManager() {
        super();
    }

    public static NotifacationManager getInstance() {
        if (instance == null) {
            synchronized (NotifacationManager.class) {
                if (instance == null) {
                    instance = new NotifacationManager();
                }
            }
        }
        return instance;
    }

    /**
     * 获取notification实例
     *
     * @param context
     * @return
     */
    public Notification getNotifactionCustomer(Context context) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.common_title_bar);// 获取remoteViews（参数一：包名；参数二：布局资源）
        Intent clickIntent = new Intent(context, ForegroundEnablingService.class);
        PendingIntent fullScreenIntent=PendingIntent.getActivity(context,CLICK_CODE,new Intent(context, ImageActivity.class),0);
        PendingIntent clickPendingIntent = PendingIntent.getBroadcast(context, CLICK_CODE, clickIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent dismiss = PendingIntent.getBroadcast(context, DISMISS_CODE, new Intent(NotificationReceiver.ACTION_DISMISS), PendingIntent.FLAG_CANCEL_CURRENT);
        Notification.Builder builder = new Notification.Builder(context.getApplicationContext())
                .setContent(remoteViews)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(false)
                .setTicker("通知已启动")
                .setContentIntent(clickPendingIntent)//设置点击intent
                .setDeleteIntent(dismiss);//设置取消广播
        // 设置自定义的Notification内容
        Notification notification = builder.build();// 获取构建好的通知--.build()最低要求在// API16及以上版本上使用，低版本上可以使用.getNotification()。
        notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT | Notification.FLAG_FOREGROUND_SERVICE;
        notification.defaults = Notification.DEFAULT_SOUND;//设置为默认的声音
        return notification;
    }

    /**
     * 获取notification实例
     *
     * @param context
     * @return
     */
    public Notification getNotifaction(Context context) {
        Intent clickIntent = new Intent(context, ForegroundEnablingService.class);
        PendingIntent clickPendingIntent = PendingIntent.getBroadcast(context, CLICK_CODE, clickIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent fullScreenIntent=PendingIntent.getActivity(context,CLICK_CODE,new Intent(context, ImageActivity.class),0);
        PendingIntent dismiss = PendingIntent.getBroadcast(context, DISMISS_CODE, new Intent(NotificationReceiver.ACTION_DISMISS), PendingIntent.FLAG_CANCEL_CURRENT);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.fengjing);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext())
                .setContentTitle("提示")
                .setContentText("定位服务进行中,请勿关闭")
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.marker))
                .setAutoCancel(false)
                .setPriority(Notification.PRIORITY_MAX)
                .setTicker("通知来啦")
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(clickPendingIntent)//设置点击intent
                .setFullScreenIntent(fullScreenIntent,true)
                .setDeleteIntent(dismiss);//设置取消广播
        // 设置自定义的Notification内容
        /**
         * 折叠文本text
         */
//        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
//        style.setBigContentTitle("点击后展开");
//        style.setSummaryText("总结");
//        style.bigText("　1.微软表示，如果用户在更新时出现设置被重置的问题，这里你可以在设置中手动进行更改。此外，微软已经开始制作相应补丁，来解决这一问题。\n" +
//                " \n" + "　　2.对于有部分用户完成周年更新升级之后所有应用(部分称只有Netflix)在打开不到1秒的时间就会崩溃的问题。\n" +
//                " \n" + "　　微软也给出了目前的临时解决方案：就是完全清除或者重置Windows Store缓存。也就是打开命令行窗口(右键Windows 10开始菜单按钮，并点击“以管理员权限打开命令行”选项)，然后输入如下命令：wreset.exe，等待数分钟时间等待完成。当进程结束之后推荐进行重启。\n" +
//                " \n" + "　　如果还是出现了应用崩溃的情况，就要联系微软团队了。\n" +
//                " \n" + "　　3.Windows Update无法下载的情况，微软也做出回应，称这一现象是因为推送为全球范围，面对的是海量的待升级设备的密集更新导致的。用户只需要等待几分钟或者使用微软的Media Creation Tool工具更新就可以了。");
//        builder.setStyle(style);

//        NotificationCompat.BigPictureStyle pictureStyle = new NotificationCompat.BigPictureStyle();
//        pictureStyle.bigPicture(BitmapFactory.decodeResource(context.getResources(), R.drawable.fengjing));
//        builder.setStyle(pictureStyle);

        Notification notification = builder.build();// 获取构建好的通知--.build()最低要求在// API16及以上版本上使用，低版本上可以使用.getNotification()。
        notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT | Notification.FLAG_FOREGROUND_SERVICE;
        return notification;
    }

    /**
     * 查询广播是否已经注册
     *
     * @param context
     * @param action
     * @return
     */
    public boolean isReceiverRegisted(Context context, String action) {
        Intent intent = new Intent();
        intent.setAction(action);
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryBroadcastReceivers(intent, 0);
        if (resolveInfos != null && !resolveInfos.isEmpty()) {
            //查询到相应的BroadcastReceiver
            return true;
        }
        return false;

    }
}
