package com.tabbar.demo.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by dick on 2017/9/11.
 */

class NotificationReceiver extends BroadcastReceiver {
    public static final String ACTION_DISMISS = "action_dismiss";
    public static final String[] ACTIONS = new String[]{
            ACTION_DISMISS,
            Intent.ACTION_BATTERY_CHANGED,
            Intent.ACTION_SCREEN_OFF,
            Intent.ACTION_SCREEN_ON
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.w("bindService", "收到广播:" + action);
        for (String ac : ACTIONS) {
            if (ac.equals(action)) {
                startService(context);
                break;
            }
        }
    }

    private void startService(Context context) {
        context.startService(new Intent(context, ForegroundEnablingService.class));
    }
}
