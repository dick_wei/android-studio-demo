package com.tabbar.demo.util;

import com.blankj.utilcode.util.LogUtils;

/**
 * Log帮助类，使用的是：import com.blankj.utilcode.util.LogUtils;
 * Created by bsh_kt on 2017/6/8.
 */
public class ToolLog {
    //    private static boolean isDebug = ConstBuild.isDebug;
    private static boolean isDebug = true;

    public static void v(Object obj) {
        if (isDebug)
            LogUtils.v(obj);
    }

    public static void v(String Tag, Object obj) {
        if (isDebug)
            LogUtils.v(Tag, obj);
    }

    public static void d(Object obj) {
        if (isDebug)
            LogUtils.d(obj);
    }

    public static void d(String Tag, Object obj) {
        if (isDebug)
            LogUtils.d(Tag, obj);
    }

    public static void i(Object obj) {
        if (isDebug)
            LogUtils.i(obj);
    }

    public static void i(String Tag, Object obj) {
        if (isDebug)
            LogUtils.i(Tag, obj);
    }

    public static void w(Object obj) {
        if (isDebug)
            LogUtils.w(obj);
    }

    public static void w(String Tag, Object obj) {
        if (isDebug)
            LogUtils.w(Tag, obj);
    }

    public static void e(Object obj) {
        if (isDebug)
            LogUtils.e(obj);
    }

    public static void e(String Tag, Object obj) {
        if (isDebug)
            LogUtils.e(Tag, obj);
    }

    public static void a(Object contents) {
        if (isDebug)
            LogUtils.a(contents);
    }

    public static void a(String tag, Object... contents) {
        if (isDebug)
            LogUtils.a(tag, contents);
    }

    public static void file(Object contents) {
        if (isDebug)
            LogUtils.file(contents);
    }

    public static void file(int type, Object contents) {
        if (isDebug)
            LogUtils.file(type, contents);
    }

    public static void file(String tag, Object contents) {
        if (isDebug)
            LogUtils.file(tag, contents);
    }

    public static void file(int type, String tag, Object contents) {
        if (isDebug)
            LogUtils.file(type, tag, contents);
    }

    public static void json(String contents) {
        if (isDebug)
            LogUtils.json(contents);
    }

    public static void json(int type, String contents) {
        if (isDebug)
            LogUtils.json(type, contents);
    }

    public static void json(String tag, String contents) {
        if (isDebug)
            LogUtils.json(tag, contents);
    }

    public static void json(int type, String tag, String contents) {
        if (isDebug)
            LogUtils.json(type, tag, contents);
    }

    public static void xml(String contents) {
        if (isDebug)
            LogUtils.xml(contents);
    }

    public static void xml(int type, String contents) {
        if (isDebug)
            LogUtils.xml(type, contents);
    }

    public static void xml(String tag, String contents) {
        if (isDebug)
            LogUtils.xml(tag, contents);
    }

    public static void xml(int type, String tag, String contents) {
        if (isDebug)
            LogUtils.xml(type, tag, contents);
    }

}
