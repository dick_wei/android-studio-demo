package com.tabbar.demo.util;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.DecimalFormat;


/**
 * Created by philipp on 02/06/16.
 */
public class YAxisValueFormatter implements IAxisValueFormatter {

    private final DecimalFormat mFormat;

    public YAxisValueFormatter() {
        mFormat = new DecimalFormat("###");
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return 1+"";
    }
}
