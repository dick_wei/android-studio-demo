package com.tabbar.demo.util;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by dick on 2017/10/19.
 */

public class CustomerPieDataFormat implements IValueFormatter, IAxisValueFormatter {
    private StringBuffer sb = new StringBuffer();
    private DecimalFormat mFormat;

    public CustomerPieDataFormat() {
        mFormat = new DecimalFormat("###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//        int num = (int) (value * 40);
//        sb.setLength(0);
//        return sb.append("反复开关门\n").append(mFormat.format(value)+"%").append(num).append("次").toString();
        ToolLog.w("chart","dataSetIndex:"+dataSetIndex+"\t value:"+value);
        PieEntry pieEntry= (PieEntry) entry;
        return ((PieEntry) entry).getLabel()+"\n\r"+mFormat.format(value) + " %";
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mFormat.format(value) + " %";
    }
}
