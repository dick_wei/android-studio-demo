package com.tabbar.demo.util;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Created by dick on 2017/10/28.
 */

public class MyDateFormater implements IValueFormatter {
    private DecimalFormat mFormat;
    public MyDateFormater() {
        mFormat = new DecimalFormat("###,###,##");
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return String.format("%d",entry.getY());
    }
}
